<MorpheusModel version="4">
    <Description>
        <Title>Cell Polarity</Title>
        <Details>Chemotaxis of polarized cell&#xd;&#xd;&#xd;
----------------------------&#xd;&#xd;&#xd;
Show feedback between cell motility, cell polarization and external gradient.&#xd;&#xd;&#xd;
 &#xd;&#xd;&#xd;
Two simple polarization models can be enabled/disabled:&#xd;&#xd;&#xd;
- Substrate-depletion model: no repolarization&#xd;&#xd;&#xd;
- Wave-pinning model: repolarization&#xd;&#xd;&#xd;
 &#xd;&#xd;&#xd;
PDE Equation:&#xd;&#xd;&#xd;
- Switch on/off and change gradients </Details>
    </Description>
    <Global>
        <Constant symbol="n" value="1"/>
        <Constant symbol="period" value="pi"/>
        <Constant symbol="osc" value="pi"/>
        <!--    <Disabled>
        <Event time-step="1.0" name="Gradient field right bound condition" trigger="on change">
            <Condition>center_cell1 > 0.9*lattice.x</Condition>
            <Rule symbol-ref="c">
                <Expression>-1*l.x/lattice.x+1+rand_uni(0,0.1)+sin(2*pi*t/osc)</Expression>
            </Rule>
        </Event>
    </Disabled>
-->
        <Field symbol="c" value="((l.x &lt; lattice.x/3) * (l.x/lattice.x)) + ((l.x >= lattice.x/3) * 1/3)" name="Gradient Field">
            <Diffusion rate="0"/>
        </Field>
        <Variable symbol="center_cell1" value="0.0"/>
        <Mapper>
            <Input value="cell.center.x * (cell.id==1)"/>
            <Output symbol-ref="center_cell1" mapping="maximum"/>
        </Mapper>
        <!--    <Disabled>
        <Event time-step="1.0" name="Gradient field left bound condition" trigger="on change">
            <Condition>center_cell1 &lt; 0.1*lattice.x</Condition>
            <Rule symbol-ref="c">
                <Expression>1*l.x/lattice.x+1+rand_uni(0,0.1)+sin(2*pi*t/osc)</Expression>
            </Rule>
        </Event>
    </Disabled>
-->
    </Global>
    <Space>
        <Lattice class="square">
            <Size symbol="lattice" value="100, 50, 0"/>
            <BoundaryConditions>
                <Condition boundary="x" type="noflux"/>
                <Condition boundary="y" type="periodic"/>
            </BoundaryConditions>
            <NodeLength value="1.0"/>
            <Neighborhood>
                <!--    <Disabled>
        <Distance>2.0</Distance>
    </Disabled>
-->
                <Order>2</Order>
            </Neighborhood>
        </Lattice>
        <SpaceSymbol symbol="l"/>
        <MembraneLattice>
            <Resolution symbol="memsize" value="50"/>
            <SpaceSymbol symbol="m"/>
        </MembraneLattice>
    </Space>
    <Time>
        <StartTime value="0"/>
        <StopTime value="150"/>
        <!--    <Disabled>
        <SaveInterval value="0"/>
    </Disabled>
-->
        <RandomSeed value="0"/>
        <TimeSymbol symbol="t"/>
    </Time>
    <CellTypes>
        <CellType class="medium" name="Medium">
            <Property symbol="l_c" value="0"/>
        </CellType>
        <CellType class="biological" name="Cell">
            <VolumeConstraint target="500" strength="0.1"/>
            <SurfaceConstraint target="1" mode="aspherity" strength="0.5"/>
            <MembraneProperty symbol="c_a" value="0.5" name="Activator concentration">
                <Diffusion rate="0"/>
            </MembraneProperty>
            <MembraneProperty symbol="c_i1" value="0.5" name="Inhibitor 1 concentration">
                <Diffusion rate="0" well-mixed="true"/>
            </MembraneProperty>
            <MembraneProperty symbol="c_i2" value="0.5" name="Inhibitor 2 concentration">
                <Diffusion rate="0.00001"/>
            </MembraneProperty>
            <Property symbol="mean_c_a" value="0.0" name="Average c_a across cell"/>
            <PropertyVector symbol="move_dir" value="0.0, 0.0, 0.0"/>
            <PropertyVector symbol="cell_polarity_vector" value="0.0, 0.0, 0.0"/>
            <Mapper>
                <Input value="c_a"/>
                <Output symbol-ref="mean_c_a" mapping="average"/>
                <Polarity symbol-ref="cell_polarity_vector"/>
            </Mapper>
            <System solver="runge-kutta" time-step="1.0" name="Meinhardt model">
                <Constant symbol="r_a" value="0.02" name="Decay rate of the activator a"/>
                <Constant symbol="b_a" value="0.1" name="Rate of the activator autocatalysis"/>
                <Constant symbol="s_a" value="0.005" name="Saturation of the activator autocatalysis"/>
                <Constant symbol="r_i1" value="0.03" name="Decay rate of inhibitor 1"/>
                <Constant symbol="b_i2" value="0.005" name="Production rate of the inhibitor 2"/>
                <Constant symbol="r_i2" value="0.013" name="Decay rate of inhibitor 2"/>
                <Constant symbol="s_c" value="0.2" name="Michaelis-Menten Constant"/>
                <Constant symbol="dy" value="0.02" name="External asymmetry"/>
                <Constant symbol="dr" value="0.01" name="Random fluctuation"/>
                <DiffEqn symbol-ref="c_a" name="Change of activator concentration">
                    <Expression>s*r_a*(c_a^2/c_i1+b_a)/(s_c+c_i2)/(1+s_a*c_a^2) - r_a*c_a</Expression>
                </DiffEqn>
                <DiffEqn symbol-ref="c_i1" name="Change of inhibitor 1 concentration">
                    <Expression>r_i1*mean_c_a*-r_i1*c_i1</Expression>
                </DiffEqn>
                <DiffEqn symbol-ref="c_i2" name="Change of inhibitor 2 concentration">
                    <Expression>b_i2*c_a-r_i2*c_i2</Expression>
                </DiffEqn>
                <Intermediate symbol="mean_c_a" value="mean_c_a"/>
            </System>
            <Mapper>
                <Input value="c_a"/>
                <Polarity symbol-ref="move_dir"/>
            </Mapper>
            <DirectedMotion direction="move_dir" strength="0.5"/>
            <Mapper name="copy c(x,y,z) to s(x,y,z)">
                <Input value="c"/>
                <Output symbol-ref="s"/>
            </Mapper>
            <MembraneProperty symbol="s" value="0" name="signal">
                <Diffusion rate="0"/>
            </MembraneProperty>
            <ConnectivityConstraint/>
        </CellType>
    </CellTypes>
    <CPM>
        <Interaction default="0.0">
            <Contact type1="Cell" type2="Medium" value="-10"/>
            <Contact type1="Cell" type2="Cell" value="-20"/>
        </Interaction>
        <MonteCarloSampler stepper="edgelist">
            <MCSDuration value="1"/>
            <Neighborhood>
                <Order>2</Order>
            </Neighborhood>
            <MetropolisKinetics temperature="1" yield="0.05"/>
        </MonteCarloSampler>
        <ShapeSurface scaling="norm">
            <Neighborhood>
                <Order>6</Order>
            </Neighborhood>
        </ShapeSurface>
    </CPM>
    <CellPopulations>
        <Population size="1" type="Cell">
            <Cell id="1" name="1">
                <Nodes>25 120 0</Nodes>
            </Cell>
        </Population>
    </CellPopulations>
    <Analysis>
        <!--    <Disabled>
        <Gnuplotter time-step="10">
            <Terminal name="png"/>
            <Plot>
                <Field symbol-ref="c"/>
                <Disabled>
                    <Cells value="A"/>
                </Disabled>
                <Cells value="c_a"/>
            </Plot>
        </Gnuplotter>
    </Disabled>
-->
        <Logger time-step="10">
            <Input>
                <Symbol symbol-ref="center_cell1"/>
                <!--    <Disabled>
        <Symbol symbol-ref="c"/>
    </Disabled>
-->
                <Symbol symbol-ref="mean_c_a"/>
            </Input>
            <Output>
                <TextOutput/>
            </Output>
        </Logger>
        <DependencyGraph reduced="false" format="svg"/>
    </Analysis>
</MorpheusModel>
